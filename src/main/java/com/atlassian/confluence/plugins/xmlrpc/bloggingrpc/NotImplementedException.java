package com.atlassian.confluence.plugins.xmlrpc.bloggingrpc;

import com.atlassian.confluence.rpc.RemoteException;

/**
 * Simple Exception for functionality that has not yet been implemented, or will
 * never be implemented.
 *
 * @since 1.2.5
 */
public class NotImplementedException extends RemoteException {

    public NotImplementedException(String arg0) {
        super(arg0);
    }
}
