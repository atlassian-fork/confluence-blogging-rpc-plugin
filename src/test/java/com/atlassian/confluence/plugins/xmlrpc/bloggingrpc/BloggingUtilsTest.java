package com.atlassian.confluence.plugins.xmlrpc.bloggingrpc;

import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.rpc.AuthenticationFailedException;
import com.atlassian.confluence.rpc.RemoteException;
import com.atlassian.confluence.security.SpacePermission;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BloggingUtilsTest {
    @Mock
    private SpaceManager spaceManager;

    @Mock
    private SpacePermissionManager spacePermissionManager;

    @Mock
    private UserAccessor userAccessor;

    @Mock
    private LocaleManager localeManager;

    @Mock
    private I18NBeanFactory i18NBeanFactory;

    @Mock
    private XhtmlContent xhtmlContent;

    @Mock
    private ConfluenceUser user;

    private BloggingUtils bloggingUtils;

    @Before
    public void setUp() {
        bloggingUtils = new DefaultBloggingUtils(userAccessor, spaceManager, spacePermissionManager, localeManager,
                i18NBeanFactory, xhtmlContent) {
            @Override
            public String getText(String s) {
                return s;
            }
        };
    }

    @After
    public void tearDown() {
        AuthenticatedUserThreadLocal.set(null);
        spaceManager = null;
        spacePermissionManager = null;
        userAccessor = null;
        localeManager = null;
        i18NBeanFactory = null;
        xhtmlContent = null;
        user = null;
    }

    @Test
    public void testSpaceListNotReturnedToAnonymousUser() {
        assertTrue(bloggingUtils.getBlogs(null).isEmpty());
        verify(spaceManager, never()).getAllSpaces();
    }

    @Test
    public void testSpaceListContainsOnlySpacesUserHasEditBlogPostAccessTo() {
        Space s1 = new Space("TST");
        Space s2 = new Space("TST2");

        when(spaceManager.getAllSpaces()).thenReturn(Arrays.asList(s1, s2));
        when(spacePermissionManager.hasPermission(SpacePermission.EDITBLOG_PERMISSION, s2, user)).thenReturn(true);

        assertEquals(singletonList(s2), bloggingUtils.getBlogs(user));
    }

    @Test
    public void testSpaceListSortedByName() {
        Space s1 = new Space("TST");
        s1.setName("B");

        Space s2 = new Space("TST2");
        s2.setName("A");

        when(spaceManager.getAllSpaces()).thenReturn(Arrays.asList(s1, s2));
        when(spacePermissionManager.hasPermission(eq(SpacePermission.EDITBLOG_PERMISSION), Matchers.anyObject(),
                eq(user))).thenReturn(true);

        assertEquals(Arrays.asList(s2, s1), bloggingUtils.getBlogs(user));
    }

    @Test
    public void testAuthenticationFailsWhenUserCannotBeFound() throws RemoteException {
        try {
            bloggingUtils.authenticateUser("", "");
            fail("Expected AuthenticationFailedException to be thrown");

        } catch (final AuthenticationFailedException auth) {
            // W00t
        }
    }

    @Test
    public void testAuthenticationFailsWhenUserNameAndPasswordDoNotMatch() throws RemoteException {
        String userName = "john.doe";
        try {
            when(userAccessor.getUserByName(userName)).thenReturn(user);

            bloggingUtils.authenticateUser(userName, "");
            fail("Expected AuthenticationFailedException to be thrown");

        } catch (final AuthenticationFailedException auth) {
            // W00t
        }
    }

    @Test
    public void testAuthenticationSuccessWhenUserExistsAndUserNameAndPasswordMatch() throws RemoteException {

        String userName = "john.doe";
        String password = "";
        try {
            when(userAccessor.getUserByName(userName)).thenReturn(user);
            when(userAccessor.authenticate(userName, password)).thenReturn(true);

            assertEquals(user, bloggingUtils.authenticateUser(userName, password));
        } catch (final AuthenticationFailedException auth) {
            // W00t
        }
    }
}
